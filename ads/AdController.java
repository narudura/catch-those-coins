package com.catchthosecoins.ads;

/**
 * Created by jarek on 21-Apr-17.
 */

public interface AdController {

    void showBanner();
    void showInterstitial();
    boolean isNetworkConnected();
}
