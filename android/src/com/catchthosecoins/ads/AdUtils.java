package com.catchthosecoins.ads;

import com.google.android.gms.ads.AdRequest;

/**
 * Created by jarek on 21-Apr-17.
 */

public class AdUtils {


    public static AdRequest buildAdRequest(){

        return new AdRequest.Builder().build();
    }


    private AdUtils(){}
}
