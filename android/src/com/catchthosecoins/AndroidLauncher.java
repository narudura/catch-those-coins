package com.catchthosecoins;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.catchthosecoins.Utils.AdController;
import com.catchthosecoins.ads.AdUnitsIds;
import com.catchthosecoins.ads.AdUtils;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

public class AndroidLauncher extends AndroidApplication implements AdController {

    private AdView bannerAdView;
    private InterstitialAd fullScreenAd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initAds();
        initUI();


    }

    private void initAds() {
        bannerAdView = new AdView(this);bannerAdView.setId(R.id.adViewId);
        bannerAdView.setAdUnitId(AdUnitsIds.BANNER_ID);
        bannerAdView.setAdSize(AdSize.SMART_BANNER);


        fullScreenAd = new InterstitialAd(this);
        fullScreenAd.setAdUnitId(AdUnitsIds.FULLSCREEN_ID);

        fullScreenAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                loadIntestitial();
            }
        });

        loadIntestitial();

    }

    private void initUI() {
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        View gameView = initializeForView(new CatchThoseCoinsGame(this), config);

        RelativeLayout layout = new RelativeLayout(this);
        RelativeLayout.LayoutParams adParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );

        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);

        RelativeLayout.LayoutParams gameParams = new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        );
        gameParams.addRule(RelativeLayout.BELOW, bannerAdView.getId());

        layout.addView(bannerAdView, adParams);
        layout.addView(gameView, gameParams);

        setContentView(layout);
    }

    @Override
    protected void onResume() {
        super.onResume();
        bannerAdView.resume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        bannerAdView.pause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bannerAdView.destroy();
    }

    @Override
    public void showBanner() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                loadBanner();
            }
        });

    }

    public void loadBanner(){
        if(isNetworkConnected()){
            bannerAdView.loadAd(AdUtils.buildAdRequest());
        }
    }

    @Override
    public void showInterstitial() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(fullScreenAd.isLoaded()){
                    fullScreenAd.show();
                }
            }
        });

    }

    public void loadIntestitial(){

        if(isNetworkConnected()){
            fullScreenAd.loadAd(AdUtils.buildAdRequest());
        }
    }

    @Override
    public boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

        return networkInfo.isConnected() && networkInfo != null;

    }
}
