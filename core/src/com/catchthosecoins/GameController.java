package com.catchthosecoins;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.catchthosecoins.ObjectBase.GameObjectsProperties;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.GameState;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.entities.Coin;
import com.catchthosecoins.entities.Obstacle;
import com.catchthosecoins.entities.PickuUps.PickupLogic.PickupsLogic;
import com.catchthosecoins.entities.Player;

/**
 * Created by jarek on 04-Apr-17.
 */
public class GameController {

    private Preferences preferences;
    private int highScore;
    private int displayHighscore;
    private static final String HIGH_SCORE_KEY = "highScore";

    private static final Logger log = new Logger(GameController.class.getName(), Logger.DEBUG);

    private Player player;
    private float playerSpeed = GameConfig.MAX_PLAYER_SPEED;
    private float tauntPlayerSpeed = -playerSpeed;
    float tauntTimer;

    private float minPositionX = Coin.getCoinBodyRadius();
    private float maxPositionX = GameConfig.WORLD_WIDTH - Coin.getCoinBodyRadius() / 2;

    private boolean obstacleSpeedSet = false;

    private PickupsLogic pickupsLogic;

    private Array<Coin> coins = new Array<Coin>();
    private float coinSpawnTimer = GameConfig.COIN_SPAWN_TIME;
    private float coinTimer;

    private int piggyCoinCounter;
    private int cometCrashCoinCounter;
    private int passedAnvilsCounter;

    private float difficultyLevel = GameConfig.EASY_COIN_SPEED;

    private Pool<Coin> coinPool;
    private int coinScore;
    private int score;

    private float obstacleStopTimer;

    private Pool<Obstacle> obstaclePool;
    private Array<Obstacle> obstacles = new Array<Obstacle>();
    private float obstacleSpawnTimer = GameConfig.OBSTACLE_SPAWN_TIME;
    private float obstacleTimer;
    private float obstacleSpeed;
    private float trueObstacleSpeed;

    private int lives = 3;

    private GameState gameState = GameState.PLAYING;

    private AssetManager assetManager;

    private Music backgroundSound;
    private Sound coinSound;
    private Sound obstacleSound;

    private CatchThoseCoinsGame game;

    public GameController(AssetManager assetManager, CatchThoseCoinsGame game) {
        this.assetManager = assetManager;
        this.game = game;
        init();
    }

    private void init() {
        preferences = Gdx.app.getPreferences(CatchThoseCoinsGame.class.getSimpleName());
        highScore = preferences.getInteger(HIGH_SCORE_KEY, 0);

        player = new Player(this);
//        player.setPosition(GameConfig.WORLD_CENTER_X, 1f);
        lives = 3;

        coinPool = Pools.get(Coin.class, 40);
        obstaclePool = Pools.get(Obstacle.class, 20);

        pickupsLogic = new PickupsLogic(this, player, assetManager);

        initSounds();

    }

    private void initSounds(){
        backgroundSound = assetManager.get(AssetDescriptors.BACKGROUND_MUSIC);
        coinSound = assetManager.get(AssetDescriptors.COIN_SOUND);
        obstacleSound = assetManager.get(AssetDescriptors.ANVIL_SOUND);
    }

    public Player getPlayer() {
        return player;
    }


    public void setObstacleSpeed(float obstacleSpeed) {
        this.obstacleSpeed = obstacleSpeed;
    }

    public void blockPlayerFromLeavingTheWorld() {

        float playerX = MathUtils.clamp(player.getX(),
                GameObjectsProperties.getPlayerBodyRadius(),
                GameConfig.WORLD_WIDTH - GameObjectsProperties.getPlayerBodyRadius());

        player.setX(playerX);
    }

    private void updatePlayer(float delta) {
        setGameOver();
        tauntTimer = pickupsLogic.getTauntTimer();


        if (gameState.isPlaying()) {

            if (tauntTimer > 0) {

                pickupsLogic.setTauntTimer(tauntTimer - delta);
                player.updatePlayer(tauntPlayerSpeed);
            } else {

                player.updatePlayer(playerSpeed);
            }

        }

        blockPlayerFromLeavingTheWorld();
    }

    public void updateWorld(float delta) {

        if (isGameOver() || gameState.isPaused()) {
            game.getAdController().showInterstitial();

        } else {
            if (pickupsLogic.getImmuneTimer() > 0) {
                pickupsLogic.decreseImmuneTimer(delta);
            }

            updatePlayer(delta);
            updateCoin(delta);
            updateObstacle(delta);
            updatePickups(delta);

        }
    }

    private void updatePickups(float delta) {
        pickupsLogic.updatePickups(delta);

    }

    private void createNewCoin(float delta) {
        coinTimer += delta;

        if (coinTimer > coinSpawnTimer) {
            float minPositionX = Coin.getCoinBodyRadius();
            float maxPositionX = GameConfig.WORLD_WIDTH - Coin.getCoinBodyRadius();

            float coinX = MathUtils.random(minPositionX, maxPositionX);
            float coinY = GameConfig.WORLD_HEIGHT;

            Coin coin = coinPool.obtain();

            coin.setCoinSpeed(difficultyLevel);
            coin.setPosition(coinX, coinY);

            coins.add(coin);


            if (difficultyLevel == GameConfig.EASY_COIN_SPEED) {
                if (coinSpawnTimer > 0.60f) {
                    coinSpawnTimer -= 0.05;
                    log.debug("Actual CoinSpawnTimer: " + coinSpawnTimer);
                }
                if (obstacleSpawnTimer > 1.20f) {
                    obstacleSpawnTimer -= 0.05;
                    log.debug("Actual ObstacleSpawnTimer: " + obstacleSpawnTimer);
                }
            } else if (difficultyLevel == GameConfig.MEDIUM_COIN_SPEED) {
                if (coinSpawnTimer > 0.40f) {
                    coinSpawnTimer -= 0.10;
                    log.debug("Actual CoinSpawnTimer: " + coinSpawnTimer);
                }
                if (obstacleSpawnTimer > 1.10f) {
                    obstacleSpawnTimer -= 0.05;
                    log.debug("Actual ObstacleSpawnTimer: " + obstacleSpawnTimer);
                }
            } else {
                if (coinSpawnTimer > 0.30f) {
                    coinSpawnTimer -= 0.10;
                    log.debug("Actual CoinSpawnTimer: " + coinSpawnTimer);
                }
                if (obstacleSpawnTimer > 0.60f) {
                    obstacleSpawnTimer -= 0.10;
                    log.debug("Actual ObstacleSpawnTimer: " + obstacleSpawnTimer);
                }

            }

            coinTimer = 0f;
        }

    }

    public void updateCoin(float delta) {

        createNewCoin(delta);
        removePassedObjects();

        for (Coin coin : coins) {
            if (coin.isNotHit() && coin.isPlayerColliding(player)) {
                coinSound.play(1f);

                coins.removeValue(coin, true);
                updateScore();
                piggyCoinCounter++;
                cometCrashCoinCounter++;
                log.debug("piggyCoins catched in a row: " + piggyCoinCounter);
                log.debug("cometCrashCoins catched in a row: " + cometCrashCoinCounter);

            }

            updateCoinsScore(delta);

            coin.updateCoin();
        }

    }

    public boolean isGameOver() {
        return gameState.isGameOver();
    }
    public boolean isPlaying() {
        return gameState.isPlaying();
    }
    public boolean isPaused() {
        return gameState.isPaused();
    }


    private void createNewObstacle(float delta) {

        if (isObstacleTimerOn()) {
            pickupsLogic.setObstacleStopTimer(pickupsLogic.getObstacleStopTimer() - delta);
            log.debug("Actual obstacleStopTimer: " + pickupsLogic.getObstacleStopTimer());
        } else {
            obstacleTimer += delta;
        }


        if (obstacleTimer > obstacleSpawnTimer) {
            float minPositionX = Obstacle.getObstacleBodyRadius();
            float maxPositionX = GameConfig.WORLD_WIDTH - Coin.getObstacleBodyRadius();

            float obstacleX = MathUtils.random(minPositionX, maxPositionX);
            float obstacleY = GameConfig.WORLD_HEIGHT;

            Obstacle obstacle = obstaclePool.obtain();
            if (isObstacleSpeedReducedOn()) {
                obstacle.setObstacleSpeed(trueObstacleSpeed / 2);
            } else {
                obstacle.setObstacleSpeed(trueObstacleSpeed);
            }

            obstacle.setPosition(obstacleX, obstacleY);

            obstacles.add(obstacle);

            if (obstacleSpawnTimer > 1.20f) {
                obstacleSpawnTimer -= 0.05;
                log.debug("Actual OBSTACLE SpawnTimer: " + obstacleSpawnTimer);
            }

            obstacleTimer = 0f;
        }

    }

    public void updateObstacle(float delta) {


        createNewObstacle(delta);
        removePassedObjects();

        for (Obstacle obstacle : obstacles) {
            if (!isObstacleSpeedReducedOn() && !obstacleSpeedSet) {
                obstacle.setObstacleSpeed(trueObstacleSpeed);
                toggleObstacleSpeedSet();
                log.debug("SPEED REDUCED OFF!");

            }
            if (pickupsLogic.getImmuneTimer() > 0) {

            } else {
                if (obstacle.isNotHit() && obstacle.isPlayerColliding(player)) {
                    obstacleSound.play(0.5f);

                    obstacles.removeValue(obstacle, true);
                    lives--;
                    passedAnvilsCounter = 0;

                    //if hit by an anvil increse counter
                    pickupsLogic.increseHitCounter();

                    passedAnvilsCounter = 0;
                    log.debug("Lives: " + lives);

                }
            }

            obstacle.updateObstacle();
        }

    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    private void removePassedObjects() {
        if (coins.size > 0) {
            Coin first = coins.first();

            if (first.getY() < -1) {
                coins.removeValue(first, true);
                coinPool.free(first);

            }
        }
        if (obstacles.size > 0) {
            Obstacle first = obstacles.first();

            if (first.getY() < -1) {
                obstacles.removeValue(first, true);

                obstaclePool.free(first);
                passedAnvilsCounter++;
                log.debug("Passed anvils: " + passedAnvilsCounter);

            }
        }


    }

    public void setReducedObstacleSpeed(float reducedObstacleSpeed) {

        pickupsLogic.setReducedSpeed(reducedObstacleSpeed);
    }

    public float getDifficultyLevel() {
        return difficultyLevel;
    }

    public Array<Coin> getCoins() {
        return coins;
    }

    public Array<Obstacle> getObstacles() {
        return obstacles;
    }

    private void updateScore() {
        score += 10;
        pickupsLogic.updateScoreForTaunt();
    }

    private void updateHighScore(){
        if(coinScore<highScore){
            return;
        }
        highScore = coinScore;
        preferences.putInteger(HIGH_SCORE_KEY, highScore);
        preferences.flush();
    }

    private void updateCoinsScore(float delta) {

        if (coinScore < score) {

            coinScore = (int) Math.min(score, coinScore + 60 * delta);

//            System.out.println("CoinScore: " + coinScore);
        }

    }

    public int getHighScore() {
        return highScore;
    }

    public void reset() {
        updateHighScore();

        pickupsLogic.reset();

        obstacleSpawnTimer = GameConfig.OBSTACLE_SPAWN_TIME;
        obstacleTimer = 0;

        coinTimer = 0;
        coinSpawnTimer = GameConfig.COIN_SPAWN_TIME;
        piggyCoinCounter = 0;
        cometCrashCoinCounter = 0;

        passedAnvilsCounter = 0;

        coinScore = 0;
        lives = 3;
        score = 0;

        tauntTimer = 0;

        coinPool.clear();
        obstaclePool.clear();
        coins.clear();
        obstacles.clear();

        player.setPosition(GameConfig.WORLD_CENTER_X, 1f);

        pickupsLogic.reset();

    }

    public void setGameOver() {

        if (lives < 1) {
            gameState = GameState.GAME_OVER;
        }
    }

    public void setDifficultyLevel(float difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public int getCoinScore() {
        return coinScore;
    }


    public float getTauntTimer() {
        return tauntTimer;
    }

    private boolean isObstacleTimerOn() {

        return pickupsLogic.getObstacleStopTimer() > 0;


    }

    public int getLives() {
        return lives;
    }


    public int getPiggyCoinCounter() {
        return piggyCoinCounter;
    }

    public void setPiggyCoinCounter(int piggyCoinCounter) {
        this.piggyCoinCounter = piggyCoinCounter;
    }

    public int getPassedAnvilsCounter() {
        return passedAnvilsCounter;
    }

    public void setPassedAnvilsCounter(int passedAnvilsCounter) {
        this.passedAnvilsCounter = passedAnvilsCounter;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public float getObstacleSpeed() {
        return obstacleSpeed;
    }

    public void incrementLives() {
        lives++;
    }

    public void toggleObstacleSpeedSet() {
        obstacleSpeedSet = !obstacleSpeedSet;

    }

    private boolean isObstacleSpeedReducedOn() {

        return pickupsLogic.getObstacleSpeedReducedTimer() > 0;
    }

    public void setTrueObstacleSpeed(float trueObstacleSpeed) {
        this.trueObstacleSpeed = trueObstacleSpeed;
    }

    public int getCometCrashCoinCounter() {
        return cometCrashCoinCounter;
    }

    public void setCometCrashCoinCounter(int cometCrashCoinCounter) {
        this.cometCrashCoinCounter = cometCrashCoinCounter;
    }

    public float getTauntPlayerSpeed() {
        return tauntPlayerSpeed;
    }

    public PickupsLogic getPickupsLogic() {
        return pickupsLogic;
    }
}
