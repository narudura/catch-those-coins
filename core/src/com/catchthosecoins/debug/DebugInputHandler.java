package com.catchthosecoins.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.utils.Logger;
import com.catchthosecoins.config.GameState;
import com.catchthosecoins.renderers.GameRenderer;

/**
 * Created by jarek on 10-Apr-17.
 */
public class DebugInputHandler {
    private static final Logger log = new Logger(DebugInputHandler.class.getName(), Logger.DEBUG);


    private GameRenderer gameRenderer;

    public DebugInputHandler(GameRenderer gameRenderer) {
        this.gameRenderer = gameRenderer;
    }

    public void inputControl() {

        if (Gdx.input.isKeyJustPressed(Input.Keys.F5)) {
                gameRenderer.toggleDebug();
                log.debug("debug: " + gameRenderer.isDebug());

        }
    }
}
