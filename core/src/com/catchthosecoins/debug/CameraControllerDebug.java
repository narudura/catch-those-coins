package com.catchthosecoins.debug;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catchthosecoins.config.GameConfig;

/**
 * Created by jarek on 04-Apr-17.
 */
public class CameraControllerDebug {

    private OrthographicCamera camera;
    private Viewport viewport;

    private Vector2 position = new Vector2();
    private Vector2 startPosition = new Vector2();

    public CameraControllerDebug() {
        init();
    }

    private void init() {
        camera = new OrthographicCamera();
        viewport = new FitViewport(GameConfig.WORLD_WIDTH, GameConfig.WORLD_HEIGHT, camera);

        startPosition.set(GameConfig.WORLD_CENTER_X, GameConfig.WORLD_CENTER_Y);
        camera.position.set(startPosition, 0);
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public Viewport getViewport() {
        return viewport;
    }


    public void cameraInputHandler(float deltaTime) {


        float moveSpeed = 5.0f * deltaTime;
        float zoomSpeed = 0.5f * deltaTime;

        //moving Camera
        if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
            moveCameraUp(moveSpeed);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.DOWN)) {
            moveCameraDown(moveSpeed);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
            moveCameraLeft(moveSpeed);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
            moveCameraRight(moveSpeed);
        }

        //Zooming camera
        if (Gdx.input.isKeyPressed(Input.Keys.PAGE_UP)) {
            zoomIn(zoomSpeed);
        }
        if (Gdx.input.isKeyPressed(Input.Keys.PAGE_DOWN)) {
            zoomOut(zoomSpeed);
        }

    }

    private void moveCameraUp(float speed) {
        position.set(camera.position.x, camera.position.y + speed);
        camera.position.set(position, 0);
    }

    private void moveCameraDown(float speed) {
        position.set(camera.position.x, camera.position.y - speed);
        camera.position.set(position, 0);
    }

    private void moveCameraLeft(float speed) {
        position.set(camera.position.x - speed, camera.position.y);
        camera.position.set(position, 0);
    }

    private void moveCameraRight(float speed) {
        position.set(camera.position.x + speed, camera.position.y);
        camera.position.set(position, 0);
    }

    private void zoomIn(float zoomSpeed){
        camera.zoom += zoomSpeed;

    }

    private void zoomOut(float zoomSpeed){
        camera.zoom -= zoomSpeed;

    }
}
