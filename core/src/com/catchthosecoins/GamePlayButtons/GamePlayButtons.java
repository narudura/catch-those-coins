package com.catchthosecoins.GamePlayButtons;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.catchthosecoins.GameController;
import com.catchthosecoins.config.assets.AssetPaths;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.GameState;
import com.catchthosecoins.entities.Player;

/**
 * Created by jarek on 16-Apr-17.
 */
public class GamePlayButtons {

    private GameController gameController;

    private Player player;
    private float playerSpeed;

    private float clickCount;


    private boolean isLeftPressed;
    private boolean isRightPressed;

    private Table table = new Table();

    private Skin skin = new Skin(Gdx.files.internal(AssetPaths.SKIN));

    private TextButton.TextButtonStyle leftButtonStyle = skin.get("left", TextButton.TextButtonStyle.class);
    private TextButton.TextButtonStyle rightButtonStyle = skin.get("right", TextButton.TextButtonStyle.class);
    private TextButton.TextButtonStyle pauseButtonStyle = skin.get("pause", TextButton.TextButtonStyle.class);

    public GamePlayButtons(GameController gameController) {
        this.gameController = gameController;

        init();
    }

    private void init() {
        skin.getFont("default-font").getData().setScale(0.4f);
        player = gameController.getPlayer();

    }

    public TextButton createButton(String title) {
        TextButton textButton = null;

        if (title.equals("Right")) {
            textButton = new TextButton("", rightButtonStyle);
            textButton.setPosition(GameConfig.HUD_WIDTH - 110, 10);
            textButton.addListener(new ClickListener() {

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    isRightPressed = true;
                    return true;
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                    isRightPressed = false;
                }
            });

        }
        if (title.equals("Left")) {
            textButton = new TextButton("", leftButtonStyle);
            textButton.setPosition(20, 10);
            textButton.addListener(new ClickListener() {

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    isLeftPressed = true;
                    return true;
                }

                @Override
                public void touchUp(InputEvent event, float x, float y, int pointer, int button) {

                    isLeftPressed = false;
                }
            });

        }
        if (title.equals("Pause")) {
            textButton = new TextButton("", pauseButtonStyle);
            textButton.setPosition(GameConfig.HUD_WIDTH / 2 - 30, 10);
            textButton.addListener(new ClickListener() {

                @Override
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                    clickCount++;
                    if (clickCount == 2) {
                        clickCount = 0;
                        gameController.setGameState(GameState.PLAYING);
                    } else {
                        gameController.setGameState(GameState.PAUSE);
                    }
                    return true;
                }
            });

        }
        return textButton;
    }

    public void buttonsInputHandling(float playerSpeed) {
        this.playerSpeed = playerSpeed;

        if (isLeftPressed) {

            player.setX(player.getX() - playerSpeed);

        } else if (isRightPressed) {

            player.setX(player.getX() + playerSpeed);
        }
    }
}
