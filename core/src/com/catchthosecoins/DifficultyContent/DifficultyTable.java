package com.catchthosecoins.DifficultyContent;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.catchthosecoins.CatchThoseCoinsGame;
import com.catchthosecoins.GameController;
import com.catchthosecoins.Screens.GameScreen;
import com.catchthosecoins.config.assets.AssetPaths;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.GameState;

/**
 * Created by jarek on 12-Apr-17.
 */
public class DifficultyTable {

    private CatchThoseCoinsGame game;
    private GameController gameController;

    private Table table = new Table();

    private Skin skin;

    private TextButton.TextButtonStyle textButtonStyle;

    public DifficultyTable(CatchThoseCoinsGame game, GameController gameController) {
        this.game = game;
        this.gameController = gameController;

        init();
    }

    private void init() {
        skin = game.getSkin();
        skin.getFont("default-font").getData().setScale(0.4f);
        textButtonStyle = skin.get("menu", TextButton.TextButtonStyle.class);

        createTable();

    }

    private void createTable() {
        table.add(createButton("Easy",textButtonStyle)).width(75).height(30).space(0, 0, 10, 0).row();
        table.add(createButton("Medium",textButtonStyle)).width(75).height(30).space(0, 0, 10, 0).row();
        table.add(createButton("Hard",textButtonStyle)).width(75).height(30);
        table.setFillParent(true);
        table.setPosition(4, -70);


    }

    private TextButton createButton(String title, TextButton.TextButtonStyle style) {

        TextButton textButton = new TextButton(title, style);

        if (title.equals("Easy")) {
            textButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    gameController.setDifficultyLevel(GameConfig.EASY_COIN_SPEED);
                    gameController.setObstacleSpeed(GameConfig.EASY_OBSTACLE_SPEED);
                    gameController.setTrueObstacleSpeed(GameConfig.EASY_OBSTACLE_SPEED);
                    gameController.setReducedObstacleSpeed(GameConfig.EASY_OBSTACLE_SPEED / 2);
                    gameController.setGameState(GameState.PLAYING);
                    gameController.reset();

                    game.setScreen(new GameScreen(game, gameController));
                }
            });

        } else if (title.equals("Medium")) {
            textButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    gameController.setDifficultyLevel(GameConfig.MEDIUM_COIN_SPEED);
                    gameController.setObstacleSpeed(GameConfig.MEDIUM_OBSTACLE_SPEED);
                    gameController.setTrueObstacleSpeed(GameConfig.MEDIUM_OBSTACLE_SPEED);
                    gameController.setReducedObstacleSpeed(GameConfig.MEDIUM_OBSTACLE_SPEED / 2);
                    gameController.setGameState(GameState.PLAYING);
                    gameController.reset();

                    game.setScreen(new GameScreen(game, gameController));
                }
            });

        } else if (title.equals("Hard")) {
            textButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    gameController.setDifficultyLevel(GameConfig.HARD_COIN_SPEED);
                    gameController.setObstacleSpeed(GameConfig.HARD_OBSTACLE_SPEED);
                    gameController.setTrueObstacleSpeed(GameConfig.HARD_OBSTACLE_SPEED);
                    gameController.setReducedObstacleSpeed(GameConfig.HARD_OBSTACLE_SPEED /2);
                    gameController.setGameState(GameState.PLAYING);
                    gameController.reset();

                    game.setScreen(new GameScreen(game, gameController));
                }
            });

        }

        return textButton;
    }

    public Table getTable() {
        return table;
    }
}
