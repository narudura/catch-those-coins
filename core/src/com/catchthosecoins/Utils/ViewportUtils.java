package com.catchthosecoins.Utils;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.Viewport;

/**
 * Created by jarek on 04-Apr-17.
 */
public class ViewportUtils {
    private static final Logger log = new Logger(ViewportUtils.class.getName(), Logger.DEBUG);

    private static final int DEFAULT_CELL_SIZE = 1;

    public static void drawGrid(Viewport viewport, ShapeRenderer shapeRenderer) {
        drawGrid(viewport, shapeRenderer, DEFAULT_CELL_SIZE);

    }

    public static void drawGrid(Viewport viewport, ShapeRenderer shapeRenderer, int cellSize) {

        if (viewport == null) {
            throw new IllegalArgumentException("We need Viewport!");
        }
        if (shapeRenderer == null) {
            throw new IllegalArgumentException("We need ShapeRenderer");
        }
        if (cellSize < DEFAULT_CELL_SIZE) {

            cellSize = DEFAULT_CELL_SIZE;
        }

        Color oldColor = new Color(shapeRenderer.getColor());

        int worldWidth = (int) viewport.getWorldWidth();
        int worldHeight = (int) viewport.getWorldHeight();

        int doubleWorldWidth = worldWidth * 2;
        int doubleWorldHeight = worldHeight * 2;

        shapeRenderer.setColor(Color.WHITE);

        //vertical lines
        for (int i = -doubleWorldWidth; i < doubleWorldWidth; i += cellSize) {

            shapeRenderer.line(i, -doubleWorldHeight, i, doubleWorldHeight);
//            log.debug("drawing line at x = " + i);

        }

        //horizontal lines
        for (int j = -doubleWorldHeight; j < doubleWorldHeight; j += cellSize) {

            shapeRenderer.line(-doubleWorldWidth, j, doubleWorldWidth, j);
        }

        shapeRenderer.setColor(Color.RED);

        shapeRenderer.line(0, -doubleWorldHeight, 0, doubleWorldHeight);
        shapeRenderer.line(-doubleWorldWidth, 0, doubleWorldWidth, 0);


    }
}
