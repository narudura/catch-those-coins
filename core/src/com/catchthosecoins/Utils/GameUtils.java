package com.catchthosecoins.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;

/**
 * Created by jarek on 04-Apr-17.
 */
public class GameUtils {

    private GameUtils(){}


    public static void clearScreen(Color color) {
        //clear screen

        Gdx.gl.glClearColor(0, 0, 0, 1.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

    }

    public static void clearScreen() {
        clearScreen(Color.BLACK);
    }
}
