package com.catchthosecoins.config;

/**
 * Created by jarek on 04-Apr-17.
 */
public class GameConfig {

    public static final float WINDOW_WIDTH = 480f; // in pixels
    public static final float WINDOW_HEIGHT = 800f;

    public static final float HUD_WIDTH = 480f; // world units (PPU)
    public static final float HUD_HEIGHT = 800f;

    public static final float WORLD_WIDTH = 6.0f; //world units
    public static final float WORLD_HEIGHT = 10.0f; //world units

    public static final float WORLD_CENTER_X = WORLD_WIDTH / 2;
    public static final float WORLD_CENTER_Y = WORLD_HEIGHT / 2;

    public static final float MAX_PLAYER_SPEED = 0.10f;
    public static final float MAX_OBSTACLE_SPEED = 0.07f;

    public static final float EASY_COIN_SPEED = 0.05f;
    public static final float MEDIUM_COIN_SPEED = 0.1f;
    public static final float HARD_COIN_SPEED = 0.15f;

    public static final float COIN_SPAWN_TIME = 2.00f;
    public static final float OBSTACLE_SPAWN_TIME = 2.50f;

    public static final float EASY_OBSTACLE_SPEED = 0.03f;
    public static final float MEDIUM_OBSTACLE_SPEED = 0.08f;
    public static final float HARD_OBSTACLE_SPEED = 0.15f;

    public static final float HEART_PICKUP_SPEED = 0.15f;
    public static final float HEART_SPAWN_TIMER = 20f;

    public static final float COMET_CRASH_PICKUP_SPEED = 0.13f;
    public static final int COMET_CRASH_COIN_NEED = 32;
    public static final int PIGGY_COIN_NEED = 20;

    public static final int OBSTACLES_PASSED_NEED = 17;

    public static final float OBSTACLE_STOP_TIMER = 15f;

    public static final float PIGGY_PICKUP_SPEED = 0.05f;
    public static final float TAUNT_PICKUP_SPEED = 0.10f;
    public static final float IMMUNE_PICKUP_SPEED = 0.15f;
    public static final float SLOW_ANVILS_PICKUP_SPEED = 0.13f;

    public static final float FIRST_STATUS_POSITION_X = 20f;
    public static final float FIRST_STATUS_POSITION_Y = HUD_HEIGHT - 50f;

    private GameConfig() {
    }
}
