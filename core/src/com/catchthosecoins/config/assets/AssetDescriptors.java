package com.catchthosecoins.config.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by jarek on 17-Apr-17.
 */
public class AssetDescriptors {

    public static final AssetDescriptor<BitmapFont> GAME_OVER_FONT = new AssetDescriptor<BitmapFont>(AssetPaths.GAME_OVER_FONT, BitmapFont.class);
    public static final AssetDescriptor<BitmapFont> COMET_CRASH_FONT = new AssetDescriptor<BitmapFont>(AssetPaths.COMET_CRASH_FONT, BitmapFont.class);
    public static final AssetDescriptor<BitmapFont> IMMUNE_FONT = new AssetDescriptor<BitmapFont>(AssetPaths.IMMUNE_FONT, BitmapFont.class);
    public static final AssetDescriptor<BitmapFont> SLOW_ANVILS_FONT = new AssetDescriptor<BitmapFont>(AssetPaths.SLOW_ANVILS_FONT, BitmapFont.class);
    public static final AssetDescriptor<BitmapFont> TAUNT_FONT = new AssetDescriptor<BitmapFont>(AssetPaths.TAUNT_FONT, BitmapFont.class);
    public static final AssetDescriptor<BitmapFont> LIVES_FONT = new AssetDescriptor<BitmapFont>(AssetPaths.LIVE_FONT, BitmapFont.class);
    public static final AssetDescriptor<BitmapFont> SCORE_FONT = new AssetDescriptor<BitmapFont>(AssetPaths.FONT_FOR_SCORE, BitmapFont.class);

    public static final AssetDescriptor<TextureAtlas> MAIN_ATLAS = new AssetDescriptor<TextureAtlas>(AssetPaths.MAIN_ATLAS, TextureAtlas.class);

    public static final AssetDescriptor<Music> INTRO_MUSIC  = new AssetDescriptor<Music>(AssetPaths.INTRO_SOUND, Music.class);
    public static final AssetDescriptor<Music> BACKGROUND_MUSIC = new AssetDescriptor<Music>(AssetPaths.BACKGROUND_SOUND, Music.class);

    public static final AssetDescriptor<Sound> COIN_SOUND = new AssetDescriptor<Sound>(AssetPaths.COIN_SOUND, Sound.class);
    public static final AssetDescriptor<Sound> ANVIL_SOUND = new AssetDescriptor<Sound>(AssetPaths.OBSTACLE_SOUND, Sound.class);
    public static final AssetDescriptor<Sound> PIGGY_SOUND = new AssetDescriptor<Sound>(AssetPaths.PIGGY_SOUND, Sound.class);
    public static final AssetDescriptor<Sound> PICKUP_SOUND = new AssetDescriptor<Sound>(AssetPaths.PICKUPS_SOUND, Sound.class);


    private AssetDescriptors(){}
}
