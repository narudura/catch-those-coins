package com.catchthosecoins.config.assets;

/**
 * Created by jarek on 17-Apr-17.
 */
public class RegionNames {

    public static final String MENU_BACKGROUND = "menuBackground";
    public static final String GAME_BACKGROUND = "dollar";

    public static final String ANVIL_OBSTACLE = "anvil";
    public static final String PLAYER = "player";
    public static final String COIN = "coin";

    public static final String HEART = "heart";
    public static final String PIGGY = "piggy";
    public static final String COMET_CRASH = "cometCrash";
    public static final String IMMUNE = "immune";
    public static final String TAUNT= "taunt";
    public static final String SLOW_ANVILS= "slowAnvils";


}
