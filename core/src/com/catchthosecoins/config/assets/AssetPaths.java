package com.catchthosecoins.config.assets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Created by jarek on 11-Apr-17.
 */
public class AssetPaths {

    public static String MAIN_ATLAS = "GamePlay/gameplay.atlas";

    public static String SKIN = "GamePlay/gameplay.json";

    public static String FONT_FOR_SCORE = "fonts/pickups/scoreCommando30.fnt";
    public static String LIVE_FONT = "fonts/pickups/commandoLives32.fnt";
    public static String GAME_OVER_FONT = "fonts/GameOver/GameOverFont.fnt";

    public static String IMMUNE_FONT = "fonts/pickups/commandoImmune24.fnt";
    public static String TAUNT_FONT = "fonts/pickups/commandoTaunt24.fnt";
    public static String SLOW_ANVILS_FONT = "fonts/pickups/commandoSlowAnvils24.fnt";
    public static String COMET_CRASH_FONT = "fonts/pickups/commandoCometCrash24.fnt";


    public static String INTRO_SOUND = "Sounds/intro.mp3";
    public static String BACKGROUND_SOUND = "Sounds/background.mp3";
    public static String COIN_SOUND = "Sounds/coin.wav";
    public static String OBSTACLE_SOUND = "Sounds/obstacle.wav";
    public static String PIGGY_SOUND = "Sounds/piggy.wav";
    public static String PICKUPS_SOUND = "Sounds/pickup.wav";
}
