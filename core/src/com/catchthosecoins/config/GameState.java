package com.catchthosecoins.config;

/**
 * Created by jarek on 10-Apr-17.
 */
public enum GameState {


    DEBUG,
    GAME_OVER,
    PLAYING,
    PAUSE;

    public boolean isDebug() {
        return this == DEBUG;
    }
    public boolean isGameOver(){
        return this == GAME_OVER;
    }
    public boolean isPlaying(){
        return this == PLAYING;
    }
    public boolean isPaused(){
        return this == PAUSE;
    }
}
