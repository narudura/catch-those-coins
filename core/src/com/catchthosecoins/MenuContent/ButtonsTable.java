package com.catchthosecoins.MenuContent;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Logger;
import com.catchthosecoins.CatchThoseCoinsGame;
import com.catchthosecoins.GameController;
import com.catchthosecoins.Screens.DifficultyScreen;
import com.catchthosecoins.Screens.GameScreen;
import com.catchthosecoins.Screens.RulesScreen;
import com.catchthosecoins.config.assets.AssetPaths;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.GameState;

/**
 * Created by jarek on 10-Apr-17.
 */
public class ButtonsTable {

    private static final Logger log = new Logger(ButtonsTable.class.getName(), Logger.DEBUG);

    private Table table = new Table();
    private Skin skin;

    private CatchThoseCoinsGame game;

    private GameController gameController;

    private TextButton.TextButtonStyle buttonStyle;


    public ButtonsTable(CatchThoseCoinsGame game, GameController gameController){
        this.game = game;
        this.gameController = gameController;
        init();
    }

    public void init(){
        skin = game.getSkin();
        skin.getFont("default-font").getData().setScale(0.4f);
        buttonStyle = skin.get("menu", TextButton.TextButtonStyle.class);

        fillTheTable();
    }


    private TextButton createButton(String title, TextButton.TextButtonStyle style){
        TextButton textButton = new TextButton(title, style);

        if(title.equals("Play")){
            textButton.addListener(new ClickListener(){
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    log.debug("Play clicked");
                    gameController.setObstacleSpeed(GameConfig.EASY_OBSTACLE_SPEED);
                    gameController.setTrueObstacleSpeed(GameConfig.EASY_OBSTACLE_SPEED);
                    gameController.setReducedObstacleSpeed(GameConfig.EASY_OBSTACLE_SPEED /2);
                    gameController.setGameState(GameState.PLAYING);
                    gameController.setDifficultyLevel(GameConfig.EASY_COIN_SPEED);
                    gameController.reset();
                    game.setScreen(new GameScreen(game, gameController));
                }
            });
        }
        if(title.equals("Quit")){
            textButton.addListener(new ClickListener(){

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    log.debug("Exit clicked");
                    Gdx.app.exit();

                }
            });
        }if(title.equals("Difficulty")){
            textButton.addListener(new ClickListener(){

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    log.debug("Difficulty clicked");
                    game.setScreen(new DifficultyScreen(game, gameController));

                }
            });
        }
        if(title.equals("Rules")){
            textButton.addListener(new ClickListener(){

                @Override
                public void clicked(InputEvent event, float x, float y) {
                    log.debug("Rules clicked");
                    game.setScreen(new RulesScreen(game, gameController));

                }
            });
        }
        return textButton;

    }

    private void fillTheTable(){
//        table.debugAll();

        table.add(createButton("Play", buttonStyle)).width(75).height(30).space(0, 0, 10, 0).row();
        table.add(createButton("Difficulty", buttonStyle)).width(75).height(30).space(0, 0, 10, 0).row();
        table.add(createButton("Quit", buttonStyle)).width(75).height(30).space(0, 0, 20, 0).row();
        table.add(createButton("Rules", buttonStyle)).width(75).height(30);
        table.setFillParent(true);
        table.setPosition(4, -110);

    }

    public Table getTable() {
        return table;
    }


}
