package com.catchthosecoins;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.catchthosecoins.Screens.MenuScreen;
import com.catchthosecoins.Utils.AdController;
import com.catchthosecoins.Utils.GameUtils;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.config.assets.AssetPaths;

public class CatchThoseCoinsGame extends Game {

	private GameController gameController;
	private AssetManager assetManager;
	private Skin skin;
	private SpriteBatch batch;

	private AdController adController;

	public CatchThoseCoinsGame(AdController adController) {
		this.adController = adController;
		adController.showBanner();
	}

	@Override
	public void create () {
		GameUtils.clearScreen();
		assetManager = new AssetManager();

		assetManager.load(AssetDescriptors.MAIN_ATLAS);

		assetManager.load(AssetDescriptors.SCORE_FONT);
		assetManager.load(AssetDescriptors.LIVES_FONT);
		assetManager.load(AssetDescriptors.GAME_OVER_FONT);
		assetManager.load(AssetDescriptors.IMMUNE_FONT);
		assetManager.load(AssetDescriptors.SLOW_ANVILS_FONT);
		assetManager.load(AssetDescriptors.COMET_CRASH_FONT);
		assetManager.load(AssetDescriptors.TAUNT_FONT);

		assetManager.load(AssetDescriptors.ANVIL_SOUND);
		assetManager.load(AssetDescriptors.COIN_SOUND);
		assetManager.load(AssetDescriptors.PIGGY_SOUND);
		assetManager.load(AssetDescriptors.PICKUP_SOUND);

		assetManager.load(AssetDescriptors.INTRO_MUSIC);
		assetManager.load(AssetDescriptors.BACKGROUND_MUSIC);

		assetManager.finishLoading();
		skin = new Skin(Gdx.files.internal(AssetPaths.SKIN));

		batch = new SpriteBatch();
		gameController = new GameController(assetManager, this);

		Gdx.app.setLogLevel(Application.LOG_DEBUG);

		setScreen(new MenuScreen(this, gameController));

	}

	@Override
	public void dispose() {
		assetManager.dispose();
		batch.dispose();
	}

	public GameController getGameController() {
		return gameController;
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

	public Skin getSkin() {
		return skin;
	}

	public SpriteBatch getBatch() {
		return batch;
	}

	public AdController getAdController() {
		return adController;
	}
}
