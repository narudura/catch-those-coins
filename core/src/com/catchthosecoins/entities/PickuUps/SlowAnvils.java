package com.catchthosecoins.entities.PickuUps;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.utils.Pool;
import com.catchthosecoins.ObjectBase.GameObjectsProperties;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.entities.Player;

/**
 * Created by jarek on 13-Apr-17.
 */
public class SlowAnvils extends GameObjectsProperties implements Pool.Poolable {

    private static final float SIZE = getPickupBodyRadius() * 2;

    private boolean hit;


    public SlowAnvils(){
        super(getPickupBodyRadius());
    }

    @Override
    public void reset() {
        hit = false;
    }

    private float slowAnvilsSpeed = GameConfig.SLOW_ANVILS_PICKUP_SPEED;
    private float slowAnvilsSpeedX = GameConfig.SLOW_ANVILS_PICKUP_SPEED /2;


    public void updateSlowAnvils() {

        setY(getY() - slowAnvilsSpeed);
        if(getX()> GameConfig.WORLD_CENTER_X - getWidth() /2){
            slowAnvilsSpeedX = -slowAnvilsSpeedX;
        }else if(getX() < 0 + getWidth() /2){
            slowAnvilsSpeedX = -slowAnvilsSpeedX;
        }

    }

    public boolean isPlayerColliding(Player player) {

        Circle playerBody = player.getBody();
        boolean overlaps = Intersector.overlaps(playerBody, getBody());

        hit = overlaps;

        return overlaps;
    }

    public void setSlowAnvilsSpeed(float slowStonesSpeed) {
        this.slowAnvilsSpeed = slowStonesSpeed;
    }

    public boolean isNotHit() {

        return !hit;
    }

    public float getWidth(){

        return SIZE;

    }

    public float getHeight(){

        return SIZE;

    }
}
