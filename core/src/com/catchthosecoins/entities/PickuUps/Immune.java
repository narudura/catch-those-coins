package com.catchthosecoins.entities.PickuUps;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.utils.Pool;
import com.catchthosecoins.ObjectBase.GameObjectsProperties;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.entities.Player;

/**
 * Created by jarek on 13-Apr-17.
 */
public class Immune extends GameObjectsProperties implements Pool.Poolable {

    private static final float SIZE = getCoinBodyRadius() * 2;

    private boolean hit;


    public Immune(){
        super(getPickupBodyRadius());
    }

    @Override
    public void reset() {
        hit = false;
    }

    private float immuneSpeed = GameConfig.IMMUNE_PICKUP_SPEED;


    public void updateImmune() {

        setY(getY() - immuneSpeed);

    }

    public boolean isPlayerColliding(Player player) {

        Circle playerBody = player.getBody();
        boolean overlaps = Intersector.overlaps(playerBody, getBody());

        hit = overlaps;

        return overlaps;
    }

    public void setImmuneSpeed(float immuneSpeed) {
        this.immuneSpeed = immuneSpeed;
    }

    public boolean isNotHit() {

        return !hit;
    }

    public float getWidth(){

        return SIZE;

    }

    public float getHeight(){

        return SIZE;

    }
}
