package com.catchthosecoins.entities.PickuUps;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.utils.Pool;
import com.catchthosecoins.ObjectBase.GameObjectsProperties;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.entities.Player;

/**
 * Created by jarek on 13-Apr-17.
 */
public class Piggy extends GameObjectsProperties implements Pool.Poolable {

    private static final float SIZE = getPickupBodyRadius() * 2;

    private boolean hit;
    private boolean reachedBorder;


    public Piggy() {
        super(getPickupBodyRadius());
    }

    @Override
    public void reset() {
        hit = false;
    }

    private float piggySpeed = GameConfig.PIGGY_PICKUP_SPEED;
    private float piggySpeedX = GameConfig.PIGGY_PICKUP_SPEED;


    public void updatePiggy() {

        setY(getY() - piggySpeed);

        if (getX() > GameConfig.WORLD_WIDTH - getWidth() / 2) {
            piggySpeedX = -piggySpeedX;
        } else if (getX() < 0 + getWidth() / 2) {
            piggySpeedX = -piggySpeedX;
        }


        setX(getX() + piggySpeedX);


    }

    public boolean isPlayerColliding(Player player) {

        Circle playerBody = player.getBody();
        boolean overlaps = Intersector.overlaps(playerBody, getBody());

        hit = overlaps;

        return overlaps;
    }

    public void setPiggySpeed(float piggySpeed) {
        this.piggySpeed = piggySpeed;
    }

    public boolean isNotHit() {

        return !hit;
    }

    public float getWidth() {

        return SIZE;

    }

    public float getHeight() {

        return SIZE;

    }
}
