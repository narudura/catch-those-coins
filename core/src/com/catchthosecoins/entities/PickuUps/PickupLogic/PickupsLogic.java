package com.catchthosecoins.entities.PickuUps.PickupLogic;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.catchthosecoins.GameController;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.entities.PickuUps.*;
import com.catchthosecoins.entities.Player;

/**
 * Created by jarek on 14-Apr-17.
 */
public class PickupsLogic {
    private static final Logger log = new Logger(PickupsLogic.class.getName(), Logger.DEBUG);

    private GameController gameController;
    private Player player;
    private float cometCrashCoinCounter;
    private float piggyCoinCounter;

    private float passedAnvilsCounter;

    private float obstacleStopTimer;

    private float immuneTimer;

    float pickupMinPositionX = HeartPickup.getPickupBodyRadius();
    float pickupMaxPositionX = GameConfig.WORLD_WIDTH - HeartPickup.getPickupBodyRadius();


    private Pool<HeartPickup> heartPickupPool;
    private Array<HeartPickup> heartPickups = new Array<HeartPickup>();
    private float heartPickupSpawnTimer = GameConfig.HEART_SPAWN_TIMER;
    private float heartPickupTimer;
    private float heartPickupSpeed = GameConfig.HEART_PICKUP_SPEED;

    private Pool<CometCrash> cometCrashPool;
    private Array<CometCrash> cometCrashes = new Array<CometCrash>();
    private float cometCrashSpeed = GameConfig.COMET_CRASH_PICKUP_SPEED;

    private Pool<Piggy> piggyPool;
    private Array<Piggy> piggies = new Array<Piggy>();
    private float piggySpeed = GameConfig.PIGGY_PICKUP_SPEED;

    private Pool<Immune> immunePool;
    private Array<Immune> immunes = new Array<Immune>();
    private float immuneSpeed = GameConfig.IMMUNE_PICKUP_SPEED;

    private Pool<SlowAnvils> slowAnvilsPool;
    private Array<SlowAnvils> slowAnvils = new Array<SlowAnvils>();
    private float slowAnvilsPickupSpeed = GameConfig.SLOW_ANVILS_PICKUP_SPEED;
    private int hitCounter;
    private float obstacleSpeedReducedTimer;

    private Pool<Taunt> tauntPool;
    private Array<Taunt> taunts = new Array<Taunt>();
    private float tauntSpeed = GameConfig.TAUNT_PICKUP_SPEED;
    private float tauntTimer;
    private int scoreForTaunt;

    private float reducedSpeed;

    private AssetManager assetManager;

    private Sound piggySound;
    private Sound pickupsSound;


    public PickupsLogic(GameController gameController, Player player, AssetManager assetManager) {
        this.gameController = gameController;
        this.player = player;
        this.assetManager = assetManager;

        init();
    }

    public void reset() {
        scoreForTaunt = 0;
        heartPickupTimer = 0;
        hitCounter = 0;

        obstacleSpeedReducedTimer = 0;
        immuneTimer = 0;
        tauntTimer = 0;
        cometCrashCoinCounter = 0;
        obstacleStopTimer = 0;


        heartPickupPool.clear();
        heartPickups.clear();

        cometCrashPool.clear();
        cometCrashes.clear();

        immunePool.clear();
        immunes.clear();

        piggyPool.clear();
        piggies.clear();

        slowAnvilsPool.clear();
        slowAnvils.clear();

        tauntPool.clear();
        taunts.clear();

    }

    public void init() {
        heartPickupPool = Pools.get(HeartPickup.class, 10);
        cometCrashPool = Pools.get(CometCrash.class, 5);
        immunePool = Pools.get(Immune.class, 5);
        piggyPool = Pools.get(Piggy.class, 5);
        slowAnvilsPool = Pools.get(SlowAnvils.class, 5);
        tauntPool = Pools.get(Taunt.class, 5);

        pickupsSound =assetManager.get(AssetDescriptors.PICKUP_SOUND);
        piggySound = assetManager.get(AssetDescriptors.PIGGY_SOUND);

    }

    public void updatePickups(float delta) {
        updateHeart(delta);
        updateCometCrash(delta);
        updateImmune(delta);
        updatePiggy(delta);
        updateSlowAnvilsPickup(delta);
        updateTaunts(delta);
    }

    public float getTauntTimer() {
        return tauntTimer;
    }

    public void setTauntTimer(float tauntTimer) {
        this.tauntTimer = tauntTimer;
    }

    public float getImmuneTimer() {
        return immuneTimer;
    }

    public void setImmuneTimer(float immuneTimer) {
        this.immuneTimer = immuneTimer;
    }

    private void createNewHeart(float delta) {
        heartPickupTimer += delta;

        if (heartPickupTimer > heartPickupSpawnTimer) {

            float heartX = MathUtils.random(pickupMinPositionX, pickupMaxPositionX);
            float heartY = GameConfig.WORLD_HEIGHT;

            HeartPickup heartPickup = heartPickupPool.obtain();

            heartPickup.setHeartSpeed(heartPickupSpeed);
            heartPickup.setPosition(heartX, heartY);

            heartPickups.add(heartPickup);
            heartPickupTimer = 0;

        }

    }

    private void createNewImmune(float delta) {
        passedAnvilsCounter = gameController.getPassedAnvilsCounter();

        if (passedAnvilsCounter == GameConfig.OBSTACLES_PASSED_NEED) {
            float immuneX = MathUtils.random(pickupMinPositionX, pickupMaxPositionX);
            float immuneY = GameConfig.WORLD_HEIGHT;

            Immune immune = immunePool.obtain();

            immune.setImmuneSpeed(immuneSpeed);
            immune.setPosition(immuneX, immuneY);

            immunes.add(immune);
            gameController.setPassedAnvilsCounter(0);
        }

    }

    private void updateImmune(float delta) {
        createNewImmune(delta);
        removePassedPickups();

        for (Immune immune : immunes) {

            if (immune.isNotHit() && immune.isPlayerColliding(player)) {
                pickupsSound.play(0.5f);
                immunes.removeValue(immune, true);

                immuneTimer = 10f;

            }

            immune.updateImmune();
        }

    }

    private void createNewPiggy(float delta) {
        piggyCoinCounter = gameController.getPiggyCoinCounter();

        if (piggyCoinCounter == GameConfig.PIGGY_COIN_NEED) {
            gameController.setPiggyCoinCounter(0);
            float piggyX = MathUtils.random(pickupMinPositionX, pickupMaxPositionX);
            float piggyY = GameConfig.WORLD_HEIGHT;

            Piggy piggy = piggyPool.obtain();

            piggy.setPiggySpeed(piggySpeed);
            piggy.setPosition(piggyX, piggyY);

            piggies.add(piggy);
        }

    }

    private void updatePiggy(float delta) {
        createNewPiggy(delta);
        removePassedPickups();

        for (Piggy piggy : piggies) {

            if (piggy.isNotHit() && piggy.isPlayerColliding(player)) {
                piggySound.play(1f);
                piggies.removeValue(piggy, true);

                gameController.setScore(gameController.getScore() + 100);

            }

            piggy.updatePiggy();
        }

    }

    private void removePassedPickups() {

        if (heartPickups.size > 0) {
            HeartPickup first = heartPickups.first();

            if (first.getY() < -1) {
                heartPickups.removeValue(first, true);
                heartPickupPool.free(first);

            }
        }
        if (cometCrashes.size > 0) {
            CometCrash first = cometCrashes.first();

            if (first.getY() < -1) {
                cometCrashes.removeValue(first, true);
                cometCrashPool.free(first);

            }
        }
        if (immunes.size > 0) {
            Immune first = immunes.first();

            if (first.getY() < -1) {
                immunes.removeValue(first, true);
                immunePool.free(first);

            }
        }
        if (piggies.size > 0) {
            Piggy first = piggies.first();

            if (first.getY() < -1) {
                piggies.removeValue(first, true);
                piggyPool.free(first);

            }
        }
        if (slowAnvils.size > 0) {
            SlowAnvils first = slowAnvils.first();

            if (first.getY() < -1) {
                slowAnvils.removeValue(first, true);
                slowAnvilsPool.free(first);

            }
        }
        if (taunts.size > 0) {
            Taunt first = taunts.first();

            if (first.getY() < -1) {
                taunts.removeValue(first, true);
                tauntPool.free(first);

            }
        }
    }

    private void createNewSlowAnvilsPickup(float delta) {

        if (hitCounter == 3) {
            float slowAnvilPickupX = MathUtils.random(pickupMinPositionX, pickupMaxPositionX);
            float slowAnvilPickupY = GameConfig.WORLD_HEIGHT;

            SlowAnvils slowAnvilsPickup = slowAnvilsPool.obtain();

            slowAnvilsPickup.setSlowAnvilsSpeed(slowAnvilsPickupSpeed);
            slowAnvilsPickup.setPosition(slowAnvilPickupX, slowAnvilPickupY);

            slowAnvils.add(slowAnvilsPickup);
            hitCounter = 0;
        }

    }

    public float getObstacleSpeedReducedTimer() {
        return obstacleSpeedReducedTimer;
    }

    private void updateSlowAnvilsPickup(float delta) {
        createNewSlowAnvilsPickup(delta);
        removePassedPickups();
        if (obstacleSpeedReducedTimer > 0) {
            obstacleSpeedReducedTimer -= delta;
            log.debug("actual ReducedTimer: " + obstacleSpeedReducedTimer);
        } else {

        }


        for (SlowAnvils slowAnvilPickup : slowAnvils) {

            if (slowAnvilPickup.isNotHit() && slowAnvilPickup.isPlayerColliding(player)) {
                pickupsSound.play(0.5f);
                gameController.toggleObstacleSpeedSet();
                slowAnvils.removeValue(slowAnvilPickup, true);
                obstacleSpeedReducedTimer = 10f;
                gameController.setObstacleSpeed(reducedSpeed);

            }


            slowAnvilPickup.updateSlowAnvils();
        }


    }

    public void setReducedSpeed(float reducedSpeed) {
        this.reducedSpeed = reducedSpeed;
    }

    private void updateHeart(float delta) {
        createNewHeart(delta);
        removePassedPickups();

        for (HeartPickup heartPickup : heartPickups) {

            if (heartPickup.isNotHit() && heartPickup.isPlayerColliding(player)) {
                pickupsSound.play(0.5f);
                heartPickups.removeValue(heartPickup, true);

                gameController.incrementLives();

                log.debug("Lives: " + gameController.getLives());

            }

            heartPickup.updateHeart();
        }

    }


    private void updateCometCrash(float delta) {
        createNewCometCrash(delta);
        removePassedPickups();

        for (CometCrash cometCrash : cometCrashes) {

            if (cometCrash.isNotHit() && cometCrash.isPlayerColliding(player)) {
                pickupsSound.play(0.5f);
                cometCrashes.removeValue(cometCrash, true);
                obstacleStopTimer = GameConfig.OBSTACLE_STOP_TIMER;

            }

            cometCrash.updateCometCrash();
        }

    }
    private void createNewCometCrash(float delta) {
        cometCrashCoinCounter = gameController.getCometCrashCoinCounter();

        if (cometCrashCoinCounter == GameConfig.COMET_CRASH_COIN_NEED) {
            gameController.setCometCrashCoinCounter(0);

            float cometX = MathUtils.random(pickupMinPositionX, pickupMaxPositionX);
            float cometY = GameConfig.WORLD_HEIGHT;

            CometCrash cometCrash = cometCrashPool.obtain();

            cometCrash.setCometCrashSpeed(cometCrashSpeed);
            cometCrash.setPosition(cometX, cometY);

            cometCrashes.add(cometCrash);

        }

    }

    private void createNewTaunt(float delta) {

        if (scoreForTaunt > 299) {
            scoreForTaunt = 0;

            float tauntX = MathUtils.random(pickupMinPositionX, pickupMaxPositionX);
            float tauntY = GameConfig.WORLD_HEIGHT;

            Taunt taunt = tauntPool.obtain();

            taunt.setTauntSpeed(tauntSpeed);
            taunt.setPosition(tauntX, tauntY);

            taunts.add(taunt);
        }

    }

    private void updateTaunts(float delta) {
        createNewTaunt(delta);
        removePassedPickups();

        for (Taunt taunt : taunts) {
            if (taunt.isNotHit() && taunt.isPlayerColliding(player)) {
                pickupsSound.play(0.5f);
                taunts.removeValue(taunt, true);
                tauntTimer = 10f;

            }

            taunt.updateTaunt();
        }

    }

    public float getObstacleStopTimer() {
        return obstacleStopTimer;
    }

    public void setObstacleStopTimer(float obstacleStopTimer) {
        this.obstacleStopTimer = obstacleStopTimer;
    }

    public Array<HeartPickup> getHeartPickups() {
        return heartPickups;
    }

    public Array<CometCrash> getCometCrashes() {
        return cometCrashes;
    }

    public Array<Immune> getImmunes() {
        return immunes;
    }

    public Array<Piggy> getPiggies() {
        return piggies;
    }

    public Array<SlowAnvils> getSlowAnvils() {
        return slowAnvils;
    }

    public Array<Taunt> getTaunts() {
        return taunts;
    }

    public void updateScoreForTaunt() {
        scoreForTaunt += 10;
    }

    public int getScoreForTaunt() {
        return scoreForTaunt;
    }

    public void decreseObstacleStopTimer(float delta) {
        obstacleStopTimer -= delta;
    }

    public void decreseImmuneTimer(float delta) {
        immuneTimer -= delta;
    }

    public void increseHitCounter() {
        hitCounter++;
    }


}
