package com.catchthosecoins.entities;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.utils.Pool;
import com.catchthosecoins.ObjectBase.GameObjectsProperties;
import com.catchthosecoins.config.GameConfig;

/**
 * Created by jarek on 04-Apr-17.
 */
public class Obstacle extends GameObjectsProperties implements Pool.Poolable {

    private static final float SIZE = getCoinBodyRadius() * 2;

    private boolean hit;

    private float obstacleSpeed = GameConfig.EASY_OBSTACLE_SPEED;

    @Override
    public void reset() {
        hit = false;

    }

    public Obstacle() {
        super(getObstacleBodyRadius());
    }


    public void updateObstacle() {

        setY(getY() - obstacleSpeed);

    }

    public boolean isPlayerColliding(Player player) {

        Circle playerBody = player.getBody();
        boolean overlaps = Intersector.overlaps(playerBody, getBody());

        hit = overlaps;

        return overlaps;
    }

    public void setObstacleSpeed(float obstacleSpeed) {
        this.obstacleSpeed = obstacleSpeed;
    }

    public boolean isNotHit() {

        return !hit;
    }

    public float getWidth(){

        return SIZE;

    }

    public float getHeight(){

        return SIZE;

    }
}
