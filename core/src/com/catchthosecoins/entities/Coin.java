package com.catchthosecoins.entities;

import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.utils.Pool;
import com.catchthosecoins.ObjectBase.GameObjectsProperties;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.entities.PickuUps.HeartPickup;

/**
 * Created by jarek on 04-Apr-17.
 */
public class Coin extends GameObjectsProperties implements Pool.Poolable {

    private static final float SIZE = getCoinBodyRadius() * 2;

    private boolean hit;


    @Override
    public void reset() {
        hit = false;

    }

    private float coinSpeed = GameConfig.EASY_COIN_SPEED;

    public Coin() {

        super(getCoinBodyRadius());
    }

    public void updateCoin() {

        setY(getY() - coinSpeed);

    }

    public boolean isPlayerColliding(Player player) {

        Circle playerBody = player.getBody();
        boolean overlaps = Intersector.overlaps(playerBody, getBody());

        hit = overlaps;

        return overlaps;
    }

    public void setCoinSpeed(float coinSpeed) {
        this.coinSpeed = coinSpeed;
    }

    public boolean isNotHit() {

        return !hit;
    }

    public float getWidth(){

        return SIZE;

    }

    public float getHeight(){

        return SIZE;

    }


}
