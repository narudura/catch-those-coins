package com.catchthosecoins.entities;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.catchthosecoins.GameController;
import com.catchthosecoins.ObjectBase.GameObjectsProperties;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.renderers.GameRenderer;

/**
 * Created by jarek on 04-Apr-17.
 */
public class Player extends GameObjectsProperties {

    private static final float SIZE = getPlayerBodyRadius() * 2;
    private static final float START_POSITION_X = GameConfig.WORLD_CENTER_X;
    private static final float START_POSITION_Y = 1f;
    private float playerSpeed = 0;

    private GameController gameController;

    public Player(GameController gameController) {

        super(getPlayerBodyRadius());
        setPosition(START_POSITION_X, START_POSITION_Y);
        this.gameController = gameController;
    }

    public void updatePlayer(float playerSpeed) {


        if (gameController.isGameOver() || gameController.isPaused()) {

        } else {
            if (Gdx.app.getType() == Application.ApplicationType.Desktop) {
                if (Gdx.input.isKeyPressed(Input.Keys.A)) {
                    moveLeft(playerSpeed);

                }

                if (Gdx.input.isKeyPressed(Input.Keys.D)) {
                    moveRight(playerSpeed);
                }
            }

            updateBody();
        }

    }

    public float getWidth() {

        return SIZE;

    }

    public float getHeight() {

        return SIZE;

    }

    public void moveLeft(float playerSpeed) {
        setX(getX() - playerSpeed);
    }

    public void moveRight(float playerSpeed) {
        setX(getX() + playerSpeed);
    }


}
