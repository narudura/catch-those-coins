package com.catchthosecoins.ObjectBase;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.entities.Coin;

/**
 * Created by jarek on 04-Apr-17.
 */
public class GameObjectsProperties {

    private float x;
    private float y;

    private static final float PLAYER_RADIUS = 0.4f;
    private static final float PLAYER_SIZE = getPlayerBodyRadius() * 2;

    private static final float COIN_RADIUS = 0.3f;
    private static final float COIN_SIZE = getCoinBodyRadius() * 2;

    private static final float PICKUP_RADIUS = 0.3f;
    private static final float PICKUP_SIZE = getPickupBodyRadius() * 2;

    private static final float OBSTACLE_RADIUS = 0.3f;
    private static final float OBSTACLE_SIZE = getCoinBodyRadius() * 2;
    private float minPositionX = Coin.getCoinBodyRadius();
    private float maxPositionX = GameConfig.WORLD_WIDTH - Coin.getCoinBodyRadius() / 2;


    private Circle body;

    public GameObjectsProperties(float bodyRadius) {
        body = new Circle(x, y, bodyRadius);
        setY(GameConfig.WORLD_HEIGHT + 1);
    }


    public static float getPlayerBodyRadius() {
        return PLAYER_RADIUS;

    }

    public static float getCoinBodyRadius() {
        return COIN_RADIUS;

    }
    public static float getPickupBodyRadius() {
        return COIN_RADIUS;

    }

    public static float getObstacleBodyRadius(){
        return OBSTACLE_RADIUS;
    }

    public void setPosition(float x, float y) {
        this.x = x;
        this.y = y;

    }

    public void updateBody() {
        body.setPosition(x, y);

    }

    public void setX(float x) {
        this.x = x;
        updateBody();

    }

    public void setY(float y) {
        this.y = y;
        updateBody();
    }

    public void drawDebug(ShapeRenderer shapeRenderer) {
        shapeRenderer.circle(body.x, body.y, body.radius, 30); // - 30 represents segments - it wont be square'ish circle

    }

    public Circle getBody() {
        return body;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }
}
