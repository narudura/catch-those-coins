package com.catchthosecoins.renderers;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catchthosecoins.GameController;
import com.catchthosecoins.GamePlayButtons.GamePlayButtons;
import com.catchthosecoins.config.assets.AssetPaths;
import com.catchthosecoins.config.GameConfig;

/**
 * Created by jarek on 04-Apr-17.
 */
public class HudRenderer {
    private static final Logger log = new Logger(HudRenderer.class.getName(), Logger.DEBUG);


    private GameController gameController;

    private SpriteBatch batch;

    private int coinScore;

    private BitmapFont fontForScore;
    private BitmapFont livesFont;
    private BitmapFont gameOverFont;

    private BitmapFont immuneFont;
    private BitmapFont tauntFont;
    private BitmapFont slowAnvilsFont;
    private BitmapFont cometCrashFont;

    private GlyphLayout layout = new GlyphLayout();
    private OrthographicCamera camera;
    private Viewport viewport;

    private GamePlayButtons gamePlayButtons;
    private Stage stage;

    private float firstStatusPositionX = GameConfig.FIRST_STATUS_POSITION_X;
    private float firstStatusPositionY = GameConfig.FIRST_STATUS_POSITION_Y;

    private float playerSpeed;

    private InputMultiplexer multiplexer;

    public HudRenderer(GameController gameController, SpriteBatch batch) {
        this.batch = batch;
        this.gameController = gameController;
        init();


    }

    private void init() {
        multiplexer = new InputMultiplexer();

        fontForScore = new BitmapFont(Gdx.files.internal(AssetPaths.FONT_FOR_SCORE));
        livesFont = new BitmapFont(Gdx.files.internal(AssetPaths.LIVE_FONT));
        gameOverFont = new BitmapFont(Gdx.files.internal(AssetPaths.GAME_OVER_FONT));

        immuneFont = new BitmapFont(Gdx.files.internal(AssetPaths.IMMUNE_FONT));
        tauntFont = new BitmapFont(Gdx.files.internal(AssetPaths.TAUNT_FONT));
        slowAnvilsFont = new BitmapFont(Gdx.files.internal(AssetPaths.SLOW_ANVILS_FONT));
        cometCrashFont = new BitmapFont(Gdx.files.internal(AssetPaths.COMET_CRASH_FONT));

        camera = new OrthographicCamera();
        viewport = new FitViewport(GameConfig.HUD_WIDTH, GameConfig.HUD_HEIGHT, camera);
        viewport.apply(true);


        if (Gdx.app.getType() == Application.ApplicationType.Android) {

            gamePlayButtons = new GamePlayButtons(gameController);
            stage = new Stage(viewport, batch);
            stage.addActor(gamePlayButtons.createButton("Left"));
            stage.addActor(gamePlayButtons.createButton("Right"));
            stage.addActor(gamePlayButtons.createButton("Pause"));

            multiplexer.addProcessor(stage);

            Gdx.input.setInputProcessor(multiplexer);
        }


    }

    public void renderHud() {

        if (gameController.getTauntTimer() > 0) {

            playerSpeed = gameController.getTauntPlayerSpeed();
        } else {

            playerSpeed = GameConfig.MAX_PLAYER_SPEED;
        }

        if (!gameController.isGameOver()) {

            if (Gdx.app.getType() == Application.ApplicationType.Android) {

                if (gameController.isPlaying()) {

                    gamePlayButtons.buttonsInputHandling(playerSpeed);
                }

                gameController.blockPlayerFromLeavingTheWorld();

                stage.act();
                stage.draw();
            }
        }

        coinScore = gameController.getCoinScore();


        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        if(gameController.isGameOver()){
            String gameOverString = "GAME OVER =(";
            layout.setText(gameOverFont, gameOverString);
            gameOverFont.draw(batch, gameOverString, GameConfig.HUD_WIDTH / 2 - layout.width / 2, GameConfig.HUD_HEIGHT / 2 + 20f);

        }

        if (gameController.isPaused()) {
            String pauseString = "PAUSED";
            layout.setText(fontForScore, pauseString);
            fontForScore.draw(batch, pauseString, GameConfig.HUD_WIDTH / 2 - layout.width / 2, GameConfig.HUD_HEIGHT / 2 + 20f);
        }

        String score = "Score: " + coinScore;
        layout.setText(fontForScore, score);
        fontForScore.draw(batch, score, 20f, GameConfig.HUD_HEIGHT - layout.height);

        String liveString = "Lives: " + gameController.getLives();
        layout.setText(livesFont, liveString);
        livesFont.draw(batch, liveString, GameConfig.HUD_WIDTH - 150f, GameConfig.HUD_HEIGHT - layout.height + 5);

        if (gameController.getPickupsLogic().getObstacleStopTimer() > 0) {
            String stopAnvilsString = "No Anvils: " + MathUtils.round(gameController.getPickupsLogic().getObstacleStopTimer()) + "s";
            layout.setText(cometCrashFont, stopAnvilsString);
            cometCrashFont.draw(batch, stopAnvilsString, firstStatusPositionX, firstStatusPositionY);
        }
        if (gameController.getPickupsLogic().getImmuneTimer() > 0) {
//            log.debug("Immune ON: " + gameController.getPickupsLogic().getImmuneTimer());
            String immuneString = "Immune: " + MathUtils.round(gameController.getPickupsLogic().getImmuneTimer()) + "s";
            layout.setText(immuneFont, immuneString);
            immuneFont.draw(batch, immuneString, firstStatusPositionX, firstStatusPositionY - (layout.height + 5f));
        }
        if (gameController.getPickupsLogic().getObstacleSpeedReducedTimer() > 0) {
            String slowAnvilsString = "Slow Anvils: " + MathUtils.round(gameController.getPickupsLogic().getObstacleSpeedReducedTimer()) + "s";
            layout.setText(slowAnvilsFont, slowAnvilsString);
            slowAnvilsFont.draw(batch, slowAnvilsString, firstStatusPositionX, firstStatusPositionY - (layout.height * 2 + 5f));
        }
        if (gameController.getPickupsLogic().getTauntTimer() > 0) {
            String tauntString = "Confuse: " + MathUtils.round(gameController.getPickupsLogic().getTauntTimer()) + "s";
            layout.setText(tauntFont, tauntString);
            tauntFont.draw(batch, tauntString, firstStatusPositionX, firstStatusPositionY - (layout.height * 3 + 5f));
        }


        batch.end();
    }

    public void dispose() {
        if(Gdx.app.getType() == Application.ApplicationType.Android) {
            stage.dispose();
        }


    }

    public void resize(int width, int height) {
        viewport.update(width, height, true);

    }


}
