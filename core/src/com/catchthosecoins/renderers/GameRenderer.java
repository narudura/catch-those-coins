package com.catchthosecoins.renderers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catchthosecoins.GameController;
import com.catchthosecoins.Utils.GameUtils;
import com.catchthosecoins.Utils.ViewportUtils;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.config.assets.RegionNames;
import com.catchthosecoins.debug.CameraControllerDebug;
import com.catchthosecoins.debug.DebugInputHandler;
import com.catchthosecoins.entities.Coin;
import com.catchthosecoins.entities.Obstacle;
import com.catchthosecoins.entities.PickuUps.*;
import com.catchthosecoins.entities.PickuUps.PickupLogic.PickupsLogic;
import com.catchthosecoins.entities.Player;

/**
 * Created by jarek on 04-Apr-17.
 */
public class GameRenderer {

    private static final Logger log = new Logger(GameRenderer.class.getName(), Logger.DEBUG);

    private boolean isDebug;
    private float elapsedTime;
    private float countAngle;
    private float countSlowAnvilsAngle;

    private Viewport viewport;
    private OrthographicCamera camera;
    private ShapeRenderer shapeRenderer;

    private CameraControllerDebug cameraControllerDebug;

    private GameController gameController;
    private Player player;

    private DebugInputHandler debugInputHandler;

    private PickupsLogic pickupsLogic;

    private TextureAtlas mainAtlas;

    private TextureRegion playerTexture;
    private TextureRegion obstacleTexture;
    private TextureRegion backgroundTexture;

    private TextureRegion heartTextureRegion;
    private TextureRegion piggyTextureRegion;
    private TextureRegion immuneTextureRegion;
    private TextureRegion tauntTextureRegion;
    private TextureRegion slowAnvilTextureRegion;
    private TextureRegion cometCrashTextureRegion;

    private SpriteBatch batch;

    private Animation<TextureRegion> coinAnimation;
    private AssetManager assetManager;

    public GameRenderer(GameController gameController, SpriteBatch batch, AssetManager assetManager) {
        this.gameController = gameController;
        this.batch = batch;
        this.assetManager = assetManager;

        init();

    }

    private void init() {

        debugInputHandler = new DebugInputHandler(this);

        cameraControllerDebug = new CameraControllerDebug();
        camera = cameraControllerDebug.getCamera();
        viewport = cameraControllerDebug.getViewport();

        shapeRenderer = new ShapeRenderer();

        player = gameController.getPlayer();
        isDebug = false;

        pickupsLogic = gameController.getPickupsLogic();

        mainAtlas = assetManager.get(AssetDescriptors.MAIN_ATLAS);
        coinAnimation = new Animation<TextureRegion>(1 / 15f, mainAtlas.findRegions(RegionNames.COIN));
        coinAnimation.setPlayMode(Animation.PlayMode.LOOP);

        playerTexture = new TextureRegion(mainAtlas.findRegion(RegionNames.PLAYER));
        playerTexture.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        obstacleTexture = new TextureRegion(mainAtlas.findRegion(RegionNames.ANVIL_OBSTACLE));
        backgroundTexture = new TextureRegion(mainAtlas.findRegion(RegionNames.GAME_BACKGROUND));

        heartTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.HEART));
        piggyTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.PIGGY));
        immuneTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.IMMUNE));
        tauntTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.TAUNT));
        slowAnvilTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.SLOW_ANVILS));
        cometCrashTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.COMET_CRASH));

        Player player = gameController.getPlayer();


    }

    public void render(float delta) {
        viewport.apply();
        GameUtils.clearScreen();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        renderGamePlay(delta);

        batch.end();

        renderDebug(delta);


        if (gameController.isGameOver()) {

        } else {
            debugInputHandler.inputControl();
            inputHandling(delta);
        }

    }

    private void renderGamePlay(float delta) {
        countAngle += delta * 50;
        countSlowAnvilsAngle += delta * 70;

        if (countAngle >= 360) {
            countAngle = 0;
        }
        if (countSlowAnvilsAngle >= 360) {
            countSlowAnvilsAngle = 0;
        }

        batch.draw(backgroundTexture, 0, 0, 6, 10);

        TextureRegion actualRegion = coinAnimation.getKeyFrame(elapsedTime, true);
        elapsedTime += delta;

        Array<Coin> coinArray = gameController.getCoins();
        for (Coin coin : coinArray) {

            batch.draw(actualRegion, coin.getX() - coin.getWidth() / 2, coin.getY() - coin.getHeight() / 2, coin.getWidth(), coin.getHeight());


        }
        Array<Obstacle> obstacleArray = gameController.getObstacles();
        for (Obstacle obstacle : obstacleArray) {

            batch.draw(obstacleTexture,
                    obstacle.getX() - obstacle.getWidth() / 2, obstacle.getY() - obstacle.getHeight() / 2,
                    obstacle.getWidth() / 2, obstacle.getHeight() / 2,
                    obstacle.getWidth(), obstacle.getHeight(),
                    1f, 1f,
                    countAngle, true);

        }

        Array<HeartPickup> heartPickups = pickupsLogic.getHeartPickups();
        for (HeartPickup heartPickup : heartPickups) {

            batch.draw(heartTextureRegion, heartPickup.getX() - heartPickup.getWidth() / 2, heartPickup.getY() - heartPickup.getHeight() / 2,
                    heartPickup.getWidth(), heartPickup.getHeight());

        }
        Array<Piggy> piggies = pickupsLogic.getPiggies();
        for (Piggy piggy : piggies) {

            batch.draw(piggyTextureRegion, piggy.getX() - piggy.getWidth() / 2, piggy.getY() - piggy.getHeight() / 2,
                    piggy.getWidth(), piggy.getHeight());

        }
        Array<Immune> immunes = pickupsLogic.getImmunes();
        for (Immune immune : immunes) {

            batch.draw(immuneTextureRegion, immune.getX() - immune.getWidth() / 2, immune.getY() - immune.getHeight() / 2,
                    immune.getWidth(), immune.getHeight());

        }
        Array<Taunt> taunts = pickupsLogic.getTaunts();
        for (Taunt taunt : taunts) {

            batch.draw(tauntTextureRegion, taunt.getX() - taunt.getWidth() / 2, taunt.getY() - taunt.getHeight() / 2,
                    taunt.getWidth(), taunt.getHeight());

        }
        Array<CometCrash> cometCrashes = pickupsLogic.getCometCrashes();
        for (CometCrash cometCrash : cometCrashes) {

            batch.draw(cometCrashTextureRegion, cometCrash.getX() - cometCrash.getWidth() / 2, cometCrash.getY() - cometCrash.getHeight() / 2,
                    cometCrash.getWidth(), cometCrash.getHeight());

        }
        Array<SlowAnvils> slowAnvils = pickupsLogic.getSlowAnvils();
        for (SlowAnvils slowAnvil : slowAnvils) {

            batch.draw(slowAnvilTextureRegion, slowAnvil.getX() - slowAnvil.getWidth() / 2, slowAnvil.getY() - slowAnvil.getHeight() / 2,
                    slowAnvil.getWidth() / 2, slowAnvil.getHeight() / 2,
                    slowAnvil.getWidth(), slowAnvil.getHeight(),
                    1f, 1f,
                    countSlowAnvilsAngle, true);

        }


        batch.draw(playerTexture, player.getX() - player.getWidth() / 2, player.getY() - player.getHeight() / 2, player.getWidth(), player.getHeight());

    }

    private void renderDebug(float delta) {
        int score = 0;

        player.updatePlayer(delta);
        gameController.updateWorld(delta);

        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);

        if (isDebug()) {
            player.drawDebug(shapeRenderer);


            Array<Coin> coinArray = gameController.getCoins();
            for (Coin coin : coinArray) {

                shapeRenderer.setColor(Color.GREEN);
                coin.drawDebug(shapeRenderer);

            }

            Array<Obstacle> obstacleArray = gameController.getObstacles();
            for (Obstacle obstacle : obstacleArray) {

                shapeRenderer.setColor(Color.RED);
                obstacle.drawDebug(shapeRenderer);

            }
            Array<HeartPickup> heartPickups = pickupsLogic.getHeartPickups();
            for (HeartPickup heartPickup : heartPickups) {

                shapeRenderer.setColor(Color.PURPLE);
                heartPickup.drawDebug(shapeRenderer);

            }
            Array<CometCrash> cometCrashes = pickupsLogic.getCometCrashes();
            for (CometCrash cometCrash : cometCrashes) {

                shapeRenderer.setColor(Color.PINK);
                cometCrash.drawDebug(shapeRenderer);

            }
            Array<Immune> immunes = pickupsLogic.getImmunes();
            for (Immune immune : immunes) {

                shapeRenderer.setColor(Color.CHARTREUSE);
                immune.drawDebug(shapeRenderer);

            }
            Array<Piggy> piggies = pickupsLogic.getPiggies();
            for (Piggy piggy : piggies) {

                shapeRenderer.setColor(Color.BLACK);
                piggy.drawDebug(shapeRenderer);

            }
            Array<SlowAnvils> slowAnvils = pickupsLogic.getSlowAnvils();
            for (SlowAnvils slowAnvilPickup : slowAnvils) {

                shapeRenderer.setColor(Color.FIREBRICK);
                slowAnvilPickup.drawDebug(shapeRenderer);

            }
            Array<Taunt> taunts = pickupsLogic.getTaunts();
            for (Taunt taunt : taunts) {

                shapeRenderer.setColor(Color.BLUE);
                taunt.drawDebug(shapeRenderer);

            }

            if (isDebug()) {
                ViewportUtils.drawGrid(viewport, shapeRenderer);
            }
        }

        shapeRenderer.end();

    }


    public void resize(int width, int height) {
        viewport.update(width, height);

    }

    private void inputHandling(float delta) {
        cameraControllerDebug.cameraInputHandler(delta);

    }


    public boolean isDebug() {
        return isDebug;
    }

    public void setDebug(boolean debug) {
        isDebug = debug;
    }

    public void toggleDebug() {
        isDebug = !isDebug;

    }

    public void dispose() {
        shapeRenderer.dispose();
    }

    public boolean isGameOver() {
        return gameController.isGameOver();
    }
}
