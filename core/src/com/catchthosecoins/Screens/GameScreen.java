package com.catchthosecoins.Screens;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.catchthosecoins.CatchThoseCoinsGame;
import com.catchthosecoins.GameController;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.renderers.GameRenderer;
import com.catchthosecoins.renderers.HudRenderer;

/**
 * Created by jarek on 04-Apr-17.
 */
public class GameScreen implements Screen {

    private AssetManager assetManager;

    private float gameOverTimeCounter = 3;
    private CatchThoseCoinsGame game;
    private GameController gameController;
    private SpriteBatch batch;

    private GameRenderer gameRenderer;
    private HudRenderer hudRenderer;

    private Music backgroundSound;

    public GameScreen(CatchThoseCoinsGame game, GameController gameController){
        this.game = game;
        this.gameController = gameController;

    }

    @Override
    public void show() {

        assetManager = game.getAssetManager();
        batch = game.getBatch();

        gameRenderer = new GameRenderer(gameController, batch, assetManager);
        hudRenderer = new HudRenderer(gameController, batch);

        backgroundSound =assetManager.get(AssetDescriptors.BACKGROUND_MUSIC);
        backgroundSound.setLooping(true);
        backgroundSound.setVolume(0.5f);
        backgroundSound.play();

    }

    @Override
    public void render(float delta) {
        gameRenderer.render(delta);
        hudRenderer.renderHud();

        if (gameRenderer.isGameOver()) {
            gameOverTimeCounter -= delta;

            if (gameOverTimeCounter <= 0){
                gameController.reset();
                game.setScreen(new MenuScreen(game, gameController));
            }

        }

    }


    @Override
    public void resize(int width, int height) {
        gameRenderer.resize(width, height);
        hudRenderer.resize(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();

    }

    @Override
    public void dispose() {
        gameRenderer.dispose();
        hudRenderer.dispose();
        backgroundSound.stop();
    }
}
