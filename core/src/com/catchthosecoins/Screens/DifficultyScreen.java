package com.catchthosecoins.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catchthosecoins.CatchThoseCoinsGame;
import com.catchthosecoins.DifficultyContent.DifficultyTable;
import com.catchthosecoins.GameController;
import com.catchthosecoins.Utils.GameUtils;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.config.assets.RegionNames;
import com.catchthosecoins.debug.CameraControllerDebug;

/**
 * Created by jarek on 12-Apr-17.
 */
public class DifficultyScreen implements Screen {

    private Viewport viewport;
    private OrthographicCamera camera;

    private DifficultyTable difficultyTable;
    private Stage stage;

    private SpriteBatch batch;

    private CatchThoseCoinsGame game;

    private InputMultiplexer multiplexer;

    private GameController gameController;

    private CameraControllerDebug cameraControllerDebug;

    private TextureAtlas atlas;

    private TextureRegion menuBackground;
    private AssetManager assetManager;

    public DifficultyScreen(CatchThoseCoinsGame game, GameController gameController) {
        this.game = game;
        this.gameController = gameController;
    }


    @Override
    public void show() {
        assetManager = game.getAssetManager();
        atlas = assetManager.get(AssetDescriptors.MAIN_ATLAS);

        menuBackground = atlas.findRegion(RegionNames.MENU_BACKGROUND);

        cameraControllerDebug = new CameraControllerDebug();
        multiplexer = new InputMultiplexer();

        camera = cameraControllerDebug.getCamera();
        viewport = new FitViewport(GameConfig.HUD_WIDTH / 2, GameConfig.HUD_HEIGHT / 2, camera);

        batch = game.getBatch();
        stage = new Stage(viewport, batch);

        difficultyTable = new DifficultyTable(game, gameController);

        stage.addActor(difficultyTable.getTable());

        multiplexer.addProcessor(stage);

        Gdx.input.setInputProcessor(multiplexer);


    }

    @Override
    public void render(float delta) {
        GameUtils.clearScreen();
        viewport.apply();
        cameraControllerDebug.cameraInputHandler(delta);
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        draw();

        batch.end();
        stage.act();
        stage.draw();

    }

    private void draw(){

        batch.draw(menuBackground, 0, 0, GameConfig.HUD_WIDTH / 2, GameConfig.HUD_HEIGHT / 2);

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        Gdx.input.setInputProcessor(null);

    }
}
