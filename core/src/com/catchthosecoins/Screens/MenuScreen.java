package com.catchthosecoins.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catchthosecoins.CatchThoseCoinsGame;
import com.catchthosecoins.GameController;
import com.catchthosecoins.MenuContent.ButtonsTable;
import com.catchthosecoins.Utils.GameUtils;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.config.assets.AssetPaths;
import com.catchthosecoins.config.assets.RegionNames;
import com.catchthosecoins.debug.CameraControllerDebug;

/**
 * Created by jarek on 04-Apr-17.
 */
public class MenuScreen implements Screen {

    private static final Logger log = new Logger(MenuScreen.class.getName(), Logger.DEBUG);

    private Viewport viewport;
    private OrthographicCamera camera;

    private ButtonsTable buttonsTable;

    private Stage stage;

    private CameraControllerDebug cameraControllerDebug;

    private SpriteBatch batch;

    private CatchThoseCoinsGame game;

    private InputMultiplexer multiplexer;

    private GameController gameController;

    private BitmapFont highScoreFont;
    private GlyphLayout layout = new GlyphLayout();


    private TextureAtlas atlas;
    private TextureRegion menuBackground;

    private Music introSound;

    private AssetManager assetManager;

    public MenuScreen(CatchThoseCoinsGame game, GameController gameController) {
        this.game = game;
        this.gameController = gameController;

    }


    @Override
    public void show() {
        assetManager = game.getAssetManager();
        atlas = assetManager.get(AssetDescriptors.MAIN_ATLAS);
        menuBackground = new TextureRegion(atlas.findRegion(RegionNames.MENU_BACKGROUND));

        introSound = assetManager.get(AssetDescriptors.INTRO_MUSIC);
        introSound.setLooping(true);
        introSound.setVolume(0.5f);
        introSound.play();


        cameraControllerDebug = new CameraControllerDebug();

        camera = cameraControllerDebug.getCamera();
        viewport = new FitViewport(GameConfig.HUD_WIDTH / 2, GameConfig.HUD_HEIGHT / 2, camera);

        batch = game.getBatch();

        buttonsTable = new ButtonsTable(game, gameController);

        stage = new Stage(viewport, batch);

        stage.addActor(buttonsTable.getTable());

        multiplexer = new InputMultiplexer();
        multiplexer.addProcessor(stage);

        highScoreFont = new BitmapFont(Gdx.files.internal(AssetPaths.LIVE_FONT));
        highScoreFont.getData().setScale(0.6f);

        Gdx.input.setInputProcessor(multiplexer);

    }

    @Override
    public void render(float delta) {
        GameUtils.clearScreen();
        viewport.apply();
        cameraControllerDebug.cameraInputHandler(delta);
        camera.update();


        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        draw();

        batch.end();

        stage.act();
        stage.draw();

    }

    private void draw() {

        batch.draw(menuBackground, 0, 0, GameConfig.HUD_WIDTH / 2, GameConfig.HUD_HEIGHT / 2);

        String highScoreString = "HIGHSCORE " + gameController.getHighScore();
        layout.setText(highScoreFont, highScoreString);
        highScoreFont.draw(batch, highScoreString, 50, 225);

    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height, true);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
        dispose();

    }

    @Override
    public void dispose() {
        introSound.stop();
        Gdx.input.setInputProcessor(null);

    }

    public Viewport getViewport() {
        return viewport;
    }

    public OrthographicCamera getCamera() {
        return camera;
    }

    public InputMultiplexer getMultiplexer() {
        return multiplexer;
    }
}
