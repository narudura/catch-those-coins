package com.catchthosecoins.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.catchthosecoins.CatchThoseCoinsGame;
import com.catchthosecoins.GameController;
import com.catchthosecoins.Utils.GameUtils;
import com.catchthosecoins.config.assets.AssetDescriptors;
import com.catchthosecoins.config.assets.AssetPaths;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.config.assets.RegionNames;
import com.catchthosecoins.debug.CameraControllerDebug;

/**
 * Created by jarek on 17-Apr-17.
 */
public class RulesScreen implements Screen {


    private Viewport viewport;
    private OrthographicCamera camera;

    private Table rulesTable;
    private Stage stage;

    private SpriteBatch batch;

    private CatchThoseCoinsGame game;

    private InputMultiplexer multiplexer;

    private GameController gameController;

    private CameraControllerDebug cameraControllerDebug;

    private AssetManager assetManager;

    private Skin skin;
    private TextButton.TextButtonStyle style;
    private BitmapFont font;

    private TextureAtlas mainAtlas;
    private TextureRegion menuBackground;

    private TextureRegion heartTextureRegion;
    private TextureRegion piggyTextureRegion;
    private TextureRegion immuneTextureRegion;
    private TextureRegion cometCrashTextureRegion;
    private TextureRegion slowAnvilsTextureRegion;
    private TextureRegion tauntTextureRegion;


    public RulesScreen(CatchThoseCoinsGame game, GameController gameController) {
        this.game = game;
        this.gameController = gameController;
    }


    @Override
    public void show() {
        assetManager = game.getAssetManager();
        skin = game.getSkin();
        style = skin.get("menu", TextButton.TextButtonStyle.class);

        font = skin.getFont("default-font");
        font.getData().setScale(0.35f);
        font.setColor(Color.BLACK);
        mainAtlas = assetManager.get(AssetDescriptors.MAIN_ATLAS);

        menuBackground = new TextureRegion(mainAtlas.findRegion(RegionNames.MENU_BACKGROUND));

        heartTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.HEART));
        piggyTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.PIGGY));
        immuneTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.IMMUNE));
        tauntTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.TAUNT));
        slowAnvilsTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.SLOW_ANVILS));
        cometCrashTextureRegion = new TextureRegion(mainAtlas.findRegion(RegionNames.COMET_CRASH));

        rulesTable = new Table();

        cameraControllerDebug = new CameraControllerDebug();
        multiplexer = new InputMultiplexer();

        camera = cameraControllerDebug.getCamera();
        viewport = new FitViewport(GameConfig.HUD_WIDTH / 2, GameConfig.HUD_HEIGHT / 2, camera);

        batch = game.getBatch();
        stage = new Stage(viewport, batch);

        TextButton backButton = new TextButton("Back", style);
        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                game.setScreen(new MenuScreen(game, gameController));
            }
        });
        rulesTable.add(backButton).width(75).height(30);
        rulesTable.setPosition(123, 30);

        stage.addActor(rulesTable);

        multiplexer.addProcessor(stage);

        Gdx.input.setInputProcessor(multiplexer);


    }

    @Override
    public void render(float delta) {
        GameUtils.clearScreen();
        viewport.apply();
        cameraControllerDebug.cameraInputHandler(delta);
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        draw();

        batch.end();

        stage.act();
        stage.draw();

    }

    private void draw() {

        batch.draw(menuBackground, 0, 0, GameConfig.HUD_WIDTH / 2, GameConfig.HUD_HEIGHT / 2);

        batch.draw(heartTextureRegion, 10, 50, heartTextureRegion.getRegionWidth() / 2, heartTextureRegion.getRegionHeight() / 2);
        String hearString = "Increse players lives by one";
        font.draw(batch, hearString, 50, 70);

        batch.draw(piggyTextureRegion, 13, 85, piggyTextureRegion.getRegionWidth() / 2, piggyTextureRegion.getRegionHeight() / 2);
        String piggyString = "Increse score by one hundred";
        font.draw(batch, piggyString, 50, 100);

        batch.draw(immuneTextureRegion, 10, 110, immuneTextureRegion.getRegionWidth() / 2, immuneTextureRegion.getRegionHeight() / 2);
        String immuneString = "Immune to anvils";
        font.draw(batch, immuneString, 50, 130);

        batch.draw(slowAnvilsTextureRegion, 10, 145, slowAnvilsTextureRegion.getRegionWidth() / 2, slowAnvilsTextureRegion.getRegionHeight() / 2);
        String slowAnvilsString = "Makes Anvils a little slower";
        font.draw(batch, slowAnvilsString, 50, 165);

        batch.draw(tauntTextureRegion, 10, 180, tauntTextureRegion.getRegionWidth() / 2, tauntTextureRegion.getRegionHeight() / 2);
        String tauntString = "Reverse movement directions";
        font.draw(batch, tauntString, 50, 200);

        batch.draw(cometCrashTextureRegion, 10, 215, cometCrashTextureRegion.getRegionWidth() / 2, cometCrashTextureRegion.getRegionHeight() / 2);
        String cometCrashString = "Stops spawning Anvils";
        font.draw(batch, cometCrashString, 50, 235);


    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
        Gdx.input.setInputProcessor(null);

    }
}


