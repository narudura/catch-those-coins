package com.catchthosecoins.desktop.ads;

import com.badlogic.gdx.utils.Logger;
import com.catchthosecoins.Utils.AdController;

/**
 * Created by jarek on 21-Apr-17.
 */

public class DesktopAdController implements AdController {

    private  static final Logger log = new Logger(DesktopAdController.class.getSimpleName());

    @Override
    public void showBanner() {

    }

    @Override
    public void showInterstitial() {

    }

    @Override
    public boolean isNetworkConnected() {
        return false;
    }
}
