package com.catchthosecoins.desktop;

import com.badlogic.gdx.tools.texturepacker.TexturePacker;

/**
 * Created by jarek on 17-Apr-17.
 */
public class AssetPacker {

    private static final String RAW_ASSETS_PATH = "desktop/assetsRAW";
    private static final String ASSETS_PATH = "android/assets/GamePlay";

    public static void main(String[] args) {
        TexturePacker.Settings settings = new TexturePacker.Settings();
        settings.combineSubdirectories = true;
        settings.flattenPaths = true;


        TexturePacker.process(settings, RAW_ASSETS_PATH, ASSETS_PATH, "gameplay");
    }
}
