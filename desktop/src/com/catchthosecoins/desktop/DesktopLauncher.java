package com.catchthosecoins.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.catchthosecoins.CatchThoseCoinsGame;
import com.catchthosecoins.Utils.AdController;
import com.catchthosecoins.config.GameConfig;
import com.catchthosecoins.desktop.ads.DesktopAdController;

public class DesktopLauncher {

	private static final AdController AD_CONTROLLER  = new DesktopAdController();
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = (int) GameConfig.WINDOW_WIDTH;
		config.height = (int) GameConfig.WINDOW_HEIGHT;
		new LwjglApplication(new CatchThoseCoinsGame(AD_CONTROLLER), config);
	}
}
